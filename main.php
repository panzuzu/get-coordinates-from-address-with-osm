<?php
/**
 * Función para obtener coordenadas desde dirección
 * Params METHOD GET:
 * email => para registro de la api
 * address values (numero, calle, ciudad) => para obtener coordenadas
 * Return:
 * array con coordenadas latitud y longitud
 */
function getCoordinatesFromAddress($email,$number_address = null, $street_address = null, $city_address = null )
{
    $url = "https://nominatim.openstreetmap.org/search?email=".$email."&q=".$number_address."+".$street_address.",+".$city_address."&format=json&polygon=1&addressdetails=1";
    $res = json_decode(file_get_contents($url));
    $coor  = [
        "lat" => $res[0]->lat,
        "lon" => $res[0]->lon
    ];
    return $coor;
}
/**
 * Función para obtener texto codificado para enviar por la url
 * Param:
 * text => agregamos el texto que vamos a ingresar en la url
 * Return:
 * string => texto codificado
 */
function adjustText($text) {
    $text = strtolower($text);
    return preg_replace('/\s+/', '+', $text);
}

/**
 * Llamamos a la api con un email para que no nos devuelva NULL
 */
print_r(
    getCoordinatesFromAddress($_GET['email'],$_GET['numero'],$_GET['calle'],$_GET['ciudad'])
);